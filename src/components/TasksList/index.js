import React from 'react';
import PropTypes from 'prop-types';
import './index.css';

const TasksList = ({
  tasks,
  toggleTaskStatus,
  editTask,
  handleEditInput,
  finishEdit,
  editedInput,
  deleteTask,
}) => (
  <table className="TasksList">
    <thead>
      <tr>
        <td className="TasksList__cell">Task</td>
        <td className="TasksList__cell">Completed</td>
        <td className="TasksList__cell">Actions</td>
      </tr>
    </thead>
    <tbody>
      {tasks.sort((a, b) => a.task < b.task).map(task => (
        <tr key={task.creationdate}>
          <td className="TasksList__cell">
            {task.beingEdited
              ? <form action="" onSubmit={finishEdit} data-creationdate={task.creationdate}>
                  <input
                    type="text"
                    value={editedInput}
                    onChange={handleEditInput}
                    data-creationdate={task.creationdate}
                  />
                </form>
              : task.task}
          </td>
          <td className="TasksList__cell">
            <button onClick={toggleTaskStatus} data-creationdate={task.creationdate}>
              {task.completed ? 'Yes' : 'No'}
            </button>
          </td>
          <td className="TasksList__cell">
            <button data-name={task.task} onClick={editTask}>Edit</button>
            <button data-name={task.task} onClick={deleteTask}>Delete</button>
          </td>
        </tr>
      ))}
    </tbody>
  </table>
);

TasksList.propTypes = {
  tasks: PropTypes.arrayOf(PropTypes.object).isRequired,
  toggleTaskStatus: PropTypes.func.isRequired,
  deleteTask: PropTypes.func.isRequired,
  editTask: PropTypes.func.isRequired,
  handleEditInput: PropTypes.func.isRequired,
  finishEdit: PropTypes.func.isRequired,
  editedInput: PropTypes.string.isRequired,
};

export default TasksList;
