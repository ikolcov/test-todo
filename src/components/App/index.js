import React, { Component } from 'react';
import logo from './logo.svg';
import AddTaskForm from '../AddTaskForm';
import TasksList from '../TasksList';
import './index.css';

class App extends Component {
  componentDidMount() {
    this.updateTasks();
    setTimeout(() =>
      this.state.tasks.forEach(task => {
        if (task.beingEdited) {
          this.setState({ editedInput: task.task });
        }
      }),
    );
  }
  state = {
    tasks: [],
    input: '',
    editedInput: '',
    error: '',
  };
  handleInput = e => {
    e.preventDefault();
    this.setState({ input: e.target.value });
  };
  updateTasks = () => this.setState({ tasks: JSON.parse(localStorage.tasks || '[]') });
  addTask = e => {
    e.preventDefault();
    try {
      this.state.tasks.forEach(task => {
        if (task.task === this.state.input) {
          throw new Error('This task already exists');
        }
      });
      if (!this.state.input) {
        throw new Error('Enter the name of task');
      }
      localStorage.setItem(
        'tasks',
        JSON.stringify([
          ...JSON.parse(localStorage.tasks || '[]'),
          {
            task: this.state.input,
            completed: false,
            beingEdited: false,
            creationdate: Date.now(),
          },
        ]),
      );
      this.updateTasks();
      this.setState({ input: '' });
    } catch (error) {
      console.log(error);
    }
  };
  toggleTaskStatus = e => {
    e.preventDefault();
    localStorage.setItem(
      'tasks',
      JSON.stringify(
        JSON.parse(localStorage.tasks || '[]').map(task => {
          if (+e.target.dataset.creationdate === task.creationdate) {
            return { ...task, completed: !task.completed };
          }
          return task;
        }),
      ),
    );
    this.updateTasks();
  };
  editTask = e => {
    e.preventDefault();
    if (this.state.editedInput) {
      return;
    }
    this.setState({ editedInput: e.target.dataset.name });
    localStorage.setItem(
      'tasks',
      JSON.stringify(
        JSON.parse(localStorage.tasks || '[]').map(task => {
          if (e.target.dataset.name === task.task) {
            return { ...task, beingEdited: !task.beingEdited };
          }
          return task;
        }),
      ),
    );
    this.updateTasks();
  };
  handleEditInput = e => {
    e.preventDefault();
    this.setState({ editedInput: e.target.value });
  };
  finishEdit = e => {
    e.preventDefault();
    localStorage.setItem(
      'tasks',
      JSON.stringify(
        JSON.parse(localStorage.tasks || '[]').map(task => {
          if (+e.target.dataset.creationdate === task.creationdate) {
            return { ...task, beingEdited: !task.beingEdited, task: this.state.editedInput };
          }
          return task;
        }),
      ),
    );
    this.updateTasks();
    this.setState({ editedInput: '' });
  };
  deleteTask = e => {
    e.preventDefault();
    localStorage.setItem(
      'tasks',
      JSON.stringify(
        JSON.parse(localStorage.tasks || '[]').filter(task => e.target.dataset.name !== task.task),
      ),
    );
    this.updateTasks();
  };

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>To-do App</h2>
        </div>
        <AddTaskForm
          input={this.state.input}
          handleInput={this.handleInput}
          addTask={this.addTask}
        />
        <TasksList
          tasks={JSON.parse(localStorage.tasks || '[]')}
          toggleTaskStatus={this.toggleTaskStatus}
          editTask={this.editTask}
          handleEditInput={this.handleEditInput}
          editedInput={this.state.editedInput}
          finishEdit={this.finishEdit}
          deleteTask={this.deleteTask}
        />
      </div>
    );
  }
}

export default App;
