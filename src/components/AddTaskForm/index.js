import React from 'react';
import PropTypes from 'prop-types';
import './index.css';

const AddTaskForm = ({ input, handleInput, addTask }) => (
  <form action="" onSubmit={addTask} className="AddTaskForm">
    <input
      type="text"
      name="task"
      placeholder="Add the next task"
      value={input}
      onChange={handleInput}
    />
    <button type="submit">Add</button>
  </form>
);

AddTaskForm.propTypes = {
  input: PropTypes.string.isRequired,
  handleInput: PropTypes.func.isRequired,
  addTask: PropTypes.func.isRequired,
};

export default AddTaskForm;
