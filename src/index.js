import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import './index.css';

function render(Component) {
  ReactDOM.render(<Component />, document.getElementById('root'));
}

render(App);

if (module.hot) {
  module.hot.accept(App, () => {
    const NextApp = App.default;
    render(NextApp);
  });
}
